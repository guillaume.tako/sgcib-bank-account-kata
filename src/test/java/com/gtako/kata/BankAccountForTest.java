package com.gtako.kata;

import java.math.BigDecimal;
import java.time.Clock;
import java.util.List;

public class BankAccountForTest extends BankAccount {

    public BankAccountForTest(Clock clock) {
        super(clock);
    }

    public BigDecimal getBalance() {
        return this.balance;
    }

    public void setBalance(BigDecimal value) {
        this.balance = value;
    }

    public List<Operation> getOperations() {
        return this.operations;
    }

    public void setOperations(List<Operation> operations) {
        this.operations.clear();
        this.operations.addAll(operations);
    }
}
