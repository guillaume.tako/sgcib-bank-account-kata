package com.gtako.kata.integration;

import com.gtako.kata.BankAccount;
import com.gtako.kata.InsufficientBalanceException;
import com.gtako.kata.NegativeAmountException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.Clock;
import java.time.Instant;
import java.time.ZoneOffset;

class BankAccountIntegrationTest {
    private final BankAccount bankAccount = new BankAccount(Clock.fixed(Instant.parse("1997-06-18T18:02:00.00Z"), ZoneOffset.UTC));

    @Test
    void bankAccountTest() throws NegativeAmountException, InsufficientBalanceException {
        // US 1 : Deposit in the account
        bankAccount.deposit(BigDecimal.valueOf(500));

        // US 2 : Withdraw from the account
        bankAccount.withdraw(BigDecimal.valueOf(42));

        // US 3 : Check the bank statement of the account
        final var actualBankStatement = bankAccount.formatBankStatement();

        String expectedBankStatement = "TYPE|AMOUNT|BALANCE|DATE" + System.lineSeparator() +
                                       "DEPOSIT|500,00|500,00|18-06-1997 18:02:00" + System.lineSeparator() +
                                       "WITHDRAWAL|42,00|458,00|18-06-1997 18:02:00" + System.lineSeparator() +
                                       "BALANCE: 458,00" + System.lineSeparator();
        Assertions.assertEquals(expectedBankStatement, actualBankStatement);
    }
}
