package com.gtako.kata.unit;

import com.gtako.kata.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.Clock;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;

class BankAccountShouldTest {
    private final Clock clock = Clock.fixed(Instant.parse("1997-06-18T17:25:00.00Z"), ZoneOffset.UTC);
    private final BankAccountForTest bankAccount = new BankAccountForTest(clock);

    // Deposit
    @Test
    void haveABalanceEqualToDepositAmountWhenDepositAmountIsPositive() throws NegativeAmountException {
        final var expectedBalance = BigDecimal.valueOf(500);

        bankAccount.deposit(expectedBalance);

        Assertions.assertEquals(expectedBalance, bankAccount.getBalance());
    }

    @Test
    void throwANegativeAmountExceptionWhenDepositAmountIsNegative() {
        Assertions.assertThrows(NegativeAmountException.class, () -> bankAccount.deposit(BigDecimal.valueOf(-500)));
    }

    // Withdrawal
    @Test
    void canWithdrawAmountGivenSufficientBalance() throws NegativeAmountException, InsufficientBalanceException {
        final var expectedBalance = BigDecimal.valueOf(377);
        bankAccount.setBalance(BigDecimal.valueOf(500));

        bankAccount.withdraw(BigDecimal.valueOf(123));

        Assertions.assertEquals(expectedBalance, bankAccount.getBalance());
    }

    @Test
    void throwAnInsufficientBalanceExceptionWhenBalanceIsInsufficient() {
        Assertions.assertThrows(InsufficientBalanceException.class, () -> bankAccount.withdraw(BigDecimal.valueOf(100)));
    }

    @Test
    void throwANegativeAmountExceptionWhenWithdrawAmountIsNegative() {
        Assertions.assertThrows(NegativeAmountException.class, () -> bankAccount.withdraw(BigDecimal.valueOf(-100)));
    }

    // BankStatement
    @Test
    void returnAFormattedBankStatementWithTheCorrectNumberOfOperations() {
        final var operations = List.of(
                new Operation(LocalDateTime.now(clock), BigDecimal.valueOf(1000), OperationType.DEPOSIT, BigDecimal.valueOf(1000)),
                new Operation(LocalDateTime.now(clock), BigDecimal.valueOf(400), OperationType.WITHDRAWAL, BigDecimal.valueOf(600)),
                new Operation(LocalDateTime.now(clock), BigDecimal.valueOf(200), OperationType.WITHDRAWAL, BigDecimal.valueOf(400))
        );

        bankAccount.setOperations(operations);
        bankAccount.setBalance(BigDecimal.valueOf(400));

        final var expectedBankStatement = "TYPE|AMOUNT|BALANCE|DATE" + System.lineSeparator() +
                                          "DEPOSIT|1000,00|1000,00|18-06-1997 17:25:00" + System.lineSeparator() +
                                          "WITHDRAWAL|400,00|600,00|18-06-1997 17:25:00" + System.lineSeparator() +
                                          "WITHDRAWAL|200,00|400,00|18-06-1997 17:25:00" + System.lineSeparator() +
                                          "BALANCE: 400,00" + System.lineSeparator();

        Assertions.assertEquals(expectedBankStatement, bankAccount.formatBankStatement());
    }
}
