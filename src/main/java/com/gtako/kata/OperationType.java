package com.gtako.kata;

public enum OperationType {
    DEPOSIT,
    WITHDRAWAL
}
