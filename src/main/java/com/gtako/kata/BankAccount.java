package com.gtako.kata;

import java.math.BigDecimal;
import java.time.Clock;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class BankAccount {
    private final Clock clock;
    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
    protected final List<Operation> operations = new ArrayList<>();
    protected BigDecimal balance = BigDecimal.ZERO;

    public BankAccount(Clock clock) {
        this.clock = clock;
    }

    public void deposit(BigDecimal amount) throws NegativeAmountException {
        if (amount.signum() < 0) {
            throw new NegativeAmountException("Deposit a negative amount is not allowed");
        }
        this.balance = balance.add(amount);
        final var operation = new Operation(LocalDateTime.now(clock), amount, OperationType.DEPOSIT, this.balance);
        operations.add(operation);
    }

    public void withdraw(BigDecimal amount) throws NegativeAmountException, InsufficientBalanceException {
        if (amount.signum() < 0) {
            throw new NegativeAmountException("Withdraw a negative amount is not allowed");
        }
        if (this.balance.compareTo(amount) < 0) {
            throw new InsufficientBalanceException("Insufficient balance to withdraw this amount of money");
        }
        this.balance = this.balance.subtract(amount);
        final var operation = new Operation(LocalDateTime.now(clock), amount, OperationType.WITHDRAWAL, this.balance);
        operations.add(operation);
    }

    public String formatBankStatement() {
        final var sb = new StringBuilder();
        sb.append("TYPE|AMOUNT|BALANCE|DATE").append(System.lineSeparator());

        for (Operation operation : operations) {
            final var type = String.format("%s|", operation.type());
            final var amount = String.format("%.2f|", operation.amount());
            final var balance = String.format("%.2f|", operation.balance());
            final var date = operation.date().format(formatter);

            sb.append(type).append(amount).append(balance).append(date).append(System.lineSeparator());
        }

        final var balance = String.format("%.2f", this.balance);

        sb.append("BALANCE: ").append(balance).append(System.lineSeparator());

        return sb.toString();
    }
}
